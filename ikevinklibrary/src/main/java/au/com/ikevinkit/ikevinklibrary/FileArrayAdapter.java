package au.com.ikevinkit.ikevinklibrary;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


public class FileArrayAdapter extends ArrayAdapter<Option> {

    private Context c;
    private int id;
    private List<Option>items;

    public FileArrayAdapter(Context context, int textViewResourceId, List<Option> objects) {
        super(context, textViewResourceId, objects);
        c = context;
        id = textViewResourceId;
        items = objects;
    }

    public Option getItem(int i) {
        return items.get(i);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(id, null);
        }
        final Option o = items.get(position);
        if (o != null) {
            TextView tvFile = (TextView) v.findViewById(R.id.TextViewName);
            if (tvFile != null) {
                tvFile.setText(o.getName());
            }
            ImageView img = (ImageView) v.findViewById(R.id.ImageViewIcon);
            if (img != null) {
                if (o.getData().equalsIgnoreCase("folder")) {
                    img.setImageResource(R.drawable.ic_folder_yellow600_36dp);
                } else {
                    /** TODO set icon based on default file association **/
                    img.setImageResource(R.drawable.ic_description_grey600_36dp);
                }
            }
            TextView tvData = (TextView) v.findViewById(R.id.TextViewData);
            if (tvData != null) {
                if (o.getData().equalsIgnoreCase("folder")) {
                    tvData.setVisibility(View.GONE);
                } else {
                    tvData.setText(o.getData());
                    tvData.setVisibility(View.VISIBLE);
                }

            }
        }
        return v;
    }

}