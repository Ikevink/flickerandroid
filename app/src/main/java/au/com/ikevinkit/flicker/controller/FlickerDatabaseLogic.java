package au.com.ikevinkit.flicker.controller;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import au.com.ikevinkit.flicker.exceptions.EmptyBookException;
import au.com.ikevinkit.flicker.exceptions.InvalidSettingException;
import au.com.ikevinkit.flicker.exceptions.NoLastDocException;
import au.com.ikevinkit.flicker.exceptions.NoWordsException;
import au.com.ikevinkit.flicker.exceptions.NotInitialisedException;
import au.com.ikevinkit.flicker.model.DbHelper;
import au.com.ikevinkit.flicker.model.FlickerWords;


/**
 * Created by Ikevink on 2/06/2015.
 */
/* Must be able to access the database
 * Saves the settings into the database and is in charge and storing and retrieving them
 * An instance of this setting class becomes the object that stores the state of the program
 */
public class FlickerDatabaseLogic {
    public static final String WPM = "WPM";
    public static final String BACKGROUNDCOLOR = "BACKGROUNDCOLOR";
    public static final String FONTCOLOR = "FONTCOLOR";
    public static final String MAXCHARACTERS = "MAXCHARACTERS";
    public static final String ORIENTATION = "ORIENTATION";
    public static final String LOADING = "LOADING";
    public static final String LASTDOCUMENT = "LASTDOCUMENT";
    public static final String PORTRAITFONTSIZE = "PORTRAITFONTSIZE";
    public static final String LANDSCAPEFONTSIZE = "LANDSCAPEFONTSIZE";



    private static DbHelper dbHelper = null;

    public static void initialise(Context context) {
        if (dbHelper == null) {
            dbHelper = new DbHelper(context);
        }
    }

    public static void addWordAt(int index, String word) throws NoWordsException {
        SQLiteDatabase writedb = dbHelper.getWritableDatabase();


        String dividedWords[] = English.splitWords(word, 13);

        for (int i = 0; i < dividedWords.length; i++) {
            // Create a new map of values, where column names are the keys
            ContentValues values = new ContentValues();
            values.put(DbHelper.CurrentBook.COLUMN_NAME_ID, index);
            values.put(DbHelper.CurrentBook.COLUMN_NAME_PIECE_INDEX, i);
            values.put(DbHelper.CurrentBook.COLUMN_NAME_WORD, word);

            // Insert the new row, returning the primary key value of the new row
            long newRowId;
            newRowId = writedb.insert(
                    DbHelper.CurrentBook.TABLE_NAME,
                    null, values);
        }
    }

    public static void eraseCurrentBook() {
        SQLiteDatabase writedb = dbHelper.getWritableDatabase();

        writedb.delete(DbHelper.CurrentBook.TABLE_NAME, null, null);
    }




    //private FlickerDatabaseLogic(DbHelper dbHelper) {
    //    this.dbHelper = dbHelper;
    //}

    public FlickerDatabaseLogic(Context context) {
        initialise(context);
    }

    private static void checkInitialised() throws NotInitialisedException {
        if (dbHelper == null) {
            throw new NotInitialisedException();
        }
    }

    public static FlickerWords getWords(int numWords) throws NotInitialisedException, EmptyBookException, NoLastDocException {
        checkInitialised();

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        int currentWordIndex = getBookProgress();

        int min = Math.max(0, currentWordIndex - (numWords / 2));
        int max = currentWordIndex + (numWords / 2);
        Cursor c = db.query(DbHelper.CurrentBook.TABLE_NAME, new String[]{DbHelper.CurrentBook.COLUMN_NAME_ID, DbHelper.CurrentBook.COLUMN_NAME_WORD},
                DbHelper.CurrentBook.COLUMN_NAME_ID + " BETWEEN ? AND ?",
                new String[] {Integer.toString(min), Integer.toString(max)},
                null, null, DbHelper.CurrentBook.COLUMN_NAME_ID);
        if (c.getCount() == 0) {
            c.close();
            throw new EmptyBookException();
        }
        FlickerWords words = new FlickerWords(c);
        c.close();
        return words;
    }

    public static String getWord() throws NotInitialisedException, EmptyBookException, NoLastDocException {
        return getWords(1).words[0];
    }

    public static String getSetting(String setting) throws NotInitialisedException, InvalidSettingException {
        checkInitialised();

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor c = db.query(DbHelper.TableSettings.TABLE_NAME,
                new String[]{DbHelper.TableSettings.COLUMN_NAME_VALUE},
                DbHelper.TableSettings.COLUMN_NAME_SETTING + " = ?",
                new String[]{setting},
                null, null, null);
        if (c.getCount() == 0) {
            c.close();
            throw new InvalidSettingException();
        }
        c.moveToFirst();
        String value = c.getString(0);
        c.close();
        return value;

    }

    public static void setSetting(String setting, int value) throws NotInitialisedException {
        setSetting(setting, Integer.toString(value));
    }

    public static void setSetting(String setting, String value) throws NotInitialisedException {
        checkInitialised();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DbHelper.TableSettings.COLUMN_NAME_SETTING, setting);
        values.put(DbHelper.TableSettings.COLUMN_NAME_VALUE, value);
        try {
            getSetting(setting);
            db.update(DbHelper.TableSettings.TABLE_NAME, values, DbHelper.TableSettings.COLUMN_NAME_SETTING + " = ?", new String[]{setting});
        } catch (InvalidSettingException ex) {
            db.insert(DbHelper.TableSettings.TABLE_NAME, null, values);
        }
    }

    public static int getFontSize(int orientation) {
        int fontsize = -1;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            try {
                fontsize = Integer.parseInt(getSetting(PORTRAITFONTSIZE));
            } catch (Exception e) {
                Log.d("getFontSize", e.getMessage());
            }
        } else {
            try {
                fontsize = Integer.parseInt(getSetting(LANDSCAPEFONTSIZE));
            } catch (Exception e) {
                Log.d("getFontSize", e.getMessage());
            }
        }
        return fontsize;
    }

    public static void setFontSize(int orientation, int fontsize) throws NotInitialisedException {
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            setSetting(PORTRAITFONTSIZE, fontsize);
        } else {
            setSetting(LANDSCAPEFONTSIZE, fontsize);
        }
    }

    public static void setLastDoc(String path) throws NotInitialisedException {
        checkInitialised();
        SQLiteDatabase writedb = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DbHelper.TableSettings.COLUMN_NAME_SETTING, LASTDOCUMENT);
        values.put(DbHelper.TableSettings.COLUMN_NAME_VALUE, path);
        writedb.update(DbHelper.TableSettings.TABLE_NAME,
                values,
                DbHelper.TableSettings.COLUMN_NAME_SETTING + " = ?",
                new String[]{LASTDOCUMENT});
    }

    public static String getLastDoc() throws NotInitialisedException, NoLastDocException {
        checkInitialised();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor c = db.query(DbHelper.TableSettings.TABLE_NAME,
                new String[]{DbHelper.TableSettings.COLUMN_NAME_VALUE},
                DbHelper.TableSettings.COLUMN_NAME_SETTING + " = ?",
                new String[]{LASTDOCUMENT},
                null, null, null);
        if (c.getCount() == 0) {
            c.close();
            throw new NoLastDocException();
        }
        c.moveToFirst();
        String value = c.getString(0);
        c.close();
        return value;
    }



    public static void newBook(String path) throws NotInitialisedException {
        checkInitialised();
        SQLiteDatabase writedb = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DbHelper.BookHistory.COLUMN_NAME_PATH, path);
        values.put(DbHelper.BookHistory.COLUMN_NAME_UPTO, 0);
        writedb.insert(DbHelper.BookHistory.TABLE_NAME, null, values);
    }

    public static void setBookProgress(int newProgress) throws NoLastDocException, NotInitialisedException {
        String lastDocument = getLastDoc();
        setBookProgress(lastDocument, newProgress);
    }

    /* You can set progress for books that you haven't read yet */
    public static void setBookProgress(String path, int newProgress) throws NotInitialisedException {
        checkInitialised();
        SQLiteDatabase writedb = dbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DbHelper.BookHistory.COLUMN_NAME_PATH, path);
        values.put(DbHelper.BookHistory.COLUMN_NAME_UPTO, newProgress);

        /*Cursor c = writedb.query(DbHelper.BookHistory.TABLE_NAME,
                new String[]{DbHelper.BookHistory.COLUMN_NAME_UPTO},
                DbHelper.BookHistory.COLUMN_NAME_PATH + " = ?",
                new String[]{path}, null, null, null);

        if (c.getCount() == 0) {
            writedb.insert(DbHelper.BookHistory.TABLE_NAME,
                    null,
                    values);
        } else {*/
            writedb.update(DbHelper.BookHistory.TABLE_NAME,
                values,
                DbHelper.BookHistory.COLUMN_NAME_PATH + " = ?",
                new String[]{path});
        //}
        //c.close();
    }

    public static int getBookProgress() throws NoLastDocException, NotInitialisedException {
        String lastDocument = getLastDoc();
        return getBookProgress(lastDocument);
    }

    public static int getBookProgress(String path) throws NotInitialisedException {
        checkInitialised();
        SQLiteDatabase writedb = dbHelper.getReadableDatabase();

        Cursor c = writedb.query(DbHelper.BookHistory.TABLE_NAME,
                new String[]{DbHelper.BookHistory.COLUMN_NAME_UPTO},
                DbHelper.BookHistory.COLUMN_NAME_PATH + " = ?",
                new String[]{path}, null, null, null);
        if (c.getCount() == 0) {
            c.close();
            ContentValues values = new ContentValues();
            values.put(DbHelper.BookHistory.COLUMN_NAME_PATH, path);
            values.put(DbHelper.BookHistory.COLUMN_NAME_UPTO, 0);

            //SQLiteDatabase writedb = dbHelper.getWritableDatabase();
            writedb.insert(DbHelper.BookHistory.TABLE_NAME,
                    null,
                    values);
            return 0;
        }
        c.moveToFirst();
        int value = Integer.parseInt(c.getString(0));
        c.close();
        return value;
    }






    public static void loadWordsIntoDatabase(String words[]) throws NotInitialisedException {
        checkInitialised();
        SQLiteDatabase writedb = dbHelper.getWritableDatabase();

        writedb.delete(DbHelper.CurrentBook.TABLE_NAME, null, null);

        for (int i = 0; i < words.length; i++) {
            // Create a new map of values, where column names are the keys
            ContentValues values = new ContentValues();
            values.put(DbHelper.CurrentBook.COLUMN_NAME_ID, i);
            values.put(DbHelper.CurrentBook.COLUMN_NAME_WORD, words[i]);

            // Insert the new row, returning the primary key value of the new row
            long newRowId;
            newRowId = writedb.insert(
                    DbHelper.CurrentBook.TABLE_NAME,
                    null, values);
        }
    }

    public static void incrementWord() throws NoLastDocException, NotInitialisedException {
        int progress = getBookProgress();
        setBookProgress(progress + 1);
    }
}
