package au.com.ikevinkit.flicker.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import au.com.ikevinkit.flicker.R;
import au.com.ikevinkit.flicker.controller.FlickerDatabaseLogic;
import au.com.ikevinkit.flicker.controller.FlickerLogic;
import au.com.ikevinkit.flicker.events.WordSelectedListener;
import au.com.ikevinkit.flicker.exceptions.EmptyBookException;
import au.com.ikevinkit.flicker.exceptions.NoLastDocException;
import au.com.ikevinkit.flicker.exceptions.NotInitialisedException;
import au.com.ikevinkit.flicker.model.DbHelper;
import au.com.ikevinkit.flicker.model.FlickerState;
import au.com.ikevinkit.flicker.model.FlickerWords;
import au.com.ikevinkit.flicker.model.OnCustomEventListener;
import au.com.ikevinkit.flicker.model.WordIndexedClickableSpan;

/**
 * Created by Ikevink on 22/06/2015.
 */
public class ScrollTextActivity extends ActionBarActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scroll_text);

        // Check whether we're recreating a previously destroyed instance
        if (savedInstanceState != null) {
            // Restore value of members from saved state
            //        int index = savedInstanceState.getInt(StateStatics.STATE_WORD_INDEX);
            //flickerState = new FlickerState(this);
        } else {
            // initialize members with default values for a new instance
            //flickerState = new FlickerState(this);
        }
        WordIndexedClickableSpan.listener = new WordSelectedListener() {
            @Override
            public void wordSelected(int wordIndex) {
                try {

                    FlickerDatabaseLogic.setBookProgress(wordIndex);
                    //Log.d("abcdef", Integer.toString(wordIndex) + ": " + FlickerDatabaseLogic.getWord());
                } catch (NoLastDocException e) {
                    e.printStackTrace();
                } catch (NotInitialisedException e) {
                    e.printStackTrace();
                //} catch (EmptyBookException e) {
                //    e.printStackTrace();
                }
                finish();
            }
        };
        updateText();
    }

    private void updateText() {
        TextView textView = (TextView) findViewById(R.id.tvTextScrollable);

        FlickerWords words;
        try {
            words = FlickerDatabaseLogic.getWords(100);
        } catch (NotInitialisedException e) {
            words = new FlickerWords(0, new String[]{"NotInitialisedException"});
        } catch (EmptyBookException e) {
            words = new FlickerWords(0, new String[]{"EmptyBookException"});
        } catch (NoLastDocException e) {
            words = new FlickerWords(0, new String[]{"NoLastDocException"});
        }

        Log.d("ScrollTextActivity", Integer.toString(words.words.length));
        StringBuilder builder = new StringBuilder();
        for(String s : words.words) {
            builder.append(s + " ");
        }
        SpannableString ss = new SpannableString(builder.toString());

        int wordStartIndex = 0;
        for (int i = 0; i < words.words.length; i++) {
            ss.setSpan(new WordIndexedClickableSpan(words.firstWordIndex + i), wordStartIndex, wordStartIndex + words.words[i].length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            wordStartIndex = wordStartIndex + words.words[i].length() + 1;
        }

        textView.setText(ss);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }
}
