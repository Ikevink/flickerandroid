package au.com.ikevinkit.flicker.controller;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import java.io.File;

import ar.com.daidalos.afiledialog.FileChooserActivity;
import ar.com.daidalos.afiledialog.FileChooserDialog;

/**
 * Created by Ikevink on 29/05/2015.
 */
public class DocumentSelector {
    Context context;


    public DocumentSelector(Context context) {
        this.context = context;

    }

    public void GetFile() {
        //Intent myIntent = new Intent(context, FileChooserActivity.class);
        //context.startActivity(myIntent);

        //toast(FileChooserActivity.I)
        FileChooserDialog dialog = new FileChooserDialog(context);
        dialog.addListener(this.onFileSelectedListener);
        dialog.show();
    }

    private FileChooserDialog.OnFileSelectedListener onFileSelectedListener = new FileChooserDialog.OnFileSelectedListener() {
        public void onFileSelected(Dialog source, File file) {
            source.hide();
            Toast toast = Toast.makeText(context, "File selected: " + file.getName(), Toast.LENGTH_LONG);
            toast.show();
        }
        public void onFileSelected(Dialog source, File folder, String name) {
            source.hide();
            Toast toast = Toast.makeText(context, "File created: " + folder.getName() + "/" + name, Toast.LENGTH_LONG);
            toast.show();
        }
    };
}
