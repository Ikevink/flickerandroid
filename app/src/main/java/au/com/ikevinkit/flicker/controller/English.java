package au.com.ikevinkit.flicker.controller;

import au.com.ikevinkit.flicker.exceptions.NoWordsException;
import au.com.ikevinkit.flicker.exceptions.NotYetImplementedException;

/* if word has a hyphen, the word should be split on the hyphen first
 * if word has capitals seemingly randomly throughout the word, split on the capitals second
 */
public class English {
    public static int getMinSplitsInWord(int numCharsInWord, int maxChars) throws NoWordsException {
        if (numCharsInWord == 0) {
            throw new NoWordsException();
        }
        //int result = 1;
        int numRemainingChars = numCharsInWord - 13;
        if (numRemainingChars <= 0) {
            return 1;
        }
        return 1 + (int)Math.ceil(numRemainingChars / 12.0);

    }

    // splits word in half or thirds, etc
    public static String[] splitWordOnMathematicallyEven(String word, int maxChars) throws NotYetImplementedException {
        throw new NotYetImplementedException();
    }

    /* in reality words should be split just before the next syllable starts that is closest to the middle of the word
     *      - complicated don't even bother implementing this at the start
     */
    public static String[] splitWordOnSyllableEvenly(String word, int maxChars) throws NotYetImplementedException {
        throw new NotYetImplementedException();
    }

    /* splits word with the first 'maxCharacters' go in the first index, next 'maxCharacters' in second
     * thus, the last split will likely end up being small
     *      - most simple method, using this first
      */
    public static String[] splitWordOnFrontLoad(String word, final int maxChars) throws NoWordsException {
        String words[] = new String[getMinSplitsInWord(word, maxChars)];
        int charIndex = 0;
        int wordIndex = 0;
        //while (charIndex < word.length()) {
        while (wordIndex < words.length) {
            if (charIndex + 13 < word.length()) {
                words[wordIndex] = word.substring(charIndex, charIndex + maxChars - 1) + "-";
                charIndex += maxChars - 1;
            } else {
                words[wordIndex] = word.substring(charIndex, word.length());
                charIndex += maxChars;
            }

            wordIndex++;
        }
        return words;
    }


    public static String[] splitWords(String word, int maxChars) throws NoWordsException {
        return splitWordOnFrontLoad(word, maxChars);
    }



    public static int getMinSplitsInWord(String word, int maxChars) throws NoWordsException {
        return getMinSplitsInWord(word.length(), maxChars);
    }


}
