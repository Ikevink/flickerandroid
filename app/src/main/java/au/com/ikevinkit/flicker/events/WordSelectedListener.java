package au.com.ikevinkit.flicker.events;

/**
 * Created by ikevink on 7/13/15.
 */
public interface WordSelectedListener {
    void wordSelected(int wordIndex);
}
