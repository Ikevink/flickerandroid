package au.com.ikevinkit.flicker.view;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

import ar.com.daidalos.afiledialog.FileChooserDialog;
import au.com.ikevinkit.flicker.R;
import au.com.ikevinkit.flicker.controller.FlickerLogic;
import au.com.ikevinkit.flicker.exceptions.EmptyBookException;
import au.com.ikevinkit.flicker.exceptions.InvalidSettingException;
import au.com.ikevinkit.flicker.exceptions.NoLastDocException;
import au.com.ikevinkit.flicker.exceptions.NotInitialisedException;
import au.com.ikevinkit.flicker.controller.FlickerDatabaseLogic;
import au.com.ikevinkit.flicker.model.FlickerState;
import au.com.ikevinkit.flicker.model.OnCustomEventListener;
import au.com.ikevinkit.ikevinklibrary.FileChooser;


public class FlickerActivity extends ActionBarActivity implements GestureDetector.OnGestureListener,GestureDetector.OnDoubleTapListener {
    static final int GET_FILE_REQUEST = 3;

    GestureDetector detector;

    FlickerState flickerState;
    FlickerLogic flickerLogic;// = new FlickerLogic();

    boolean startingActivity = false;
    public void startScrollTextActivity() {
        if (!startingActivity) {
            startingActivity = true;
            startActivity(new Intent(this, ScrollTextActivity.class));
        }
    }
    public void startSettingsActivity() {
        if (!startingActivity) {
            startingActivity = true;
            startActivity(new Intent(this, SettingsActivity.class));
        }
    }
    public void startFileChooserActivity() {
        if (!startingActivity) {
            startingActivity = true;
            Intent intent = new Intent(this, FileChooser.class);
            startActivityForResult(intent, GET_FILE_REQUEST);
        }
    }


    private void loadSettings() {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);

        int backcolour = sharedPrefs.getInt(FlickerDatabaseLogic.BACKGROUNDCOLOR, Color.WHITE);
        findViewById(R.id.background).setBackgroundColor(backcolour);
        int fontcolour = sharedPrefs.getInt(FlickerDatabaseLogic.FONTCOLOR, Color.BLACK);
        //((TextView)findViewById(R.id.tvWord)).setTextColor(fontcolour);
        ((TextView)findViewById(R.id.tvWordLeft)).setTextColor(fontcolour);
        ((TextView)findViewById(R.id.tvWordRight)).setTextColor(fontcolour);
        int orientation = sharedPrefs.getInt(FlickerDatabaseLogic.ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_SENSOR);
        if (orientation == ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        } else if (orientation == ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
        }

        flickerState.loadSettings(sharedPrefs);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Check whether we're recreating a previously destroyed instance
        if (savedInstanceState != null) {
            // Restore value of members from saved state
    //        int index = savedInstanceState.getInt(StateStatics.STATE_WORD_INDEX);
            flickerState = new FlickerState(this);
        } else {
            // initialize members with default values for a new instance
            flickerState = new FlickerState(this);
        }

        //mDetector = new GestureDetectorCompat(this, this);


        flickerLogic = new FlickerLogic(flickerState);

        detector = new GestureDetector(this, this);

        ((LinearLayout)findViewById(R.id.layWord)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
                //flickerLogic.FlickerPlayPauseToggle();
                //toast("abc");
            }
        });

        flickerState.wordChangedListener(new OnCustomEventListener() {
            public void onEvent() {
                updateText();
            }
        });

        ((LinearLayout)findViewById(R.id.layWord)).setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                detector.onTouchEvent(event);
                return false;
            }
        });
        loadSettings();



        ((Button)findViewById(R.id.btnTest)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startScrollTextActivity();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    private void updateText() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String splitWord[] = new String[]{"", "Loading...", ""};
                boolean loading = false;
                try {
                    loading = FlickerDatabaseLogic.getSetting(FlickerDatabaseLogic.LOADING).equals("T");
                } catch (NotInitialisedException e) {
                    FlickerDatabaseLogic.initialise(FlickerActivity.this);
                } catch (InvalidSettingException e) {
                    e.printStackTrace();
                }
                try {
                    splitWord = flickerLogic.getNextWordSplit();
                } catch (NotInitialisedException e) {
                    FlickerDatabaseLogic.initialise(FlickerActivity.this);
                } catch (EmptyBookException e) {
                    if (loading) {
                        splitWord = new String[]{"", "Loading...", ""};
                    } else {
                        splitWord = new String[]{"", "Empty Book", ""};
                    }
                } catch (NoLastDocException e) {
                    //e.printStackTrace();
                    splitWord = new String[]{"", "Haven't selected a doc", ""};
                }
                final TextView left = (TextView) findViewById(R.id.tvWordLeft);
                left.setText(splitWord[0]);
                final TextView middle = (TextView) findViewById(R.id.tvWordMiddle);
                middle.setText(splitWord[1]);
                final TextView right = (TextView) findViewById(R.id.tvWordRight);
                right.setText(splitWord[2]);
            }
        });
    }

    @Override
    protected void onResume() {
        startingActivity = false;
        updateText();
        super.onResume();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // this probably should be done onPause but anyway
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()) {
            case R.id.action_settings:
                startSettingsActivity();
                return true;
            case R.id.action_browse:
                startFileChooserActivity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



    // Call Back method  to get the Message form other Activity    override the method
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);


        // check if the request code is same as what is passed  here it is 2
        if(requestCode==GET_FILE_REQUEST) {
            if (resultCode == RESULT_OK) {
                String path = data.getStringExtra("path");
                try {
                    File file = new File(path);
                    final TextView left = (TextView) findViewById(R.id.tvWordLeft);
                    left.setText("");
                    final TextView middle = (TextView) findViewById(R.id.tvWordMiddle);
                    middle.setText("Loading...");
                    final TextView right = (TextView) findViewById(R.id.tvWordRight);
                    right.setText("");
                    flickerLogic.loadFile(file);    /* push this into the background */
                    //flickerState.loadSavedState();
                } catch (NotInitialisedException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public void toast(final String message) {
        final Activity activity = this;
        runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return true;
    }

    @Override
    public void onShowPress(MotionEvent e) {
    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        //if (!flickerLogic.isSpeedReading()) {
        //    toast(Float.toString(distanceY) + " y distance");
        //}
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        //if (!flickerLogic.isSpeedReading()) {
        //    toast(Float.toString(velocityY) + " y velocity");
        //}
        return true;
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent e) {
        flickerLogic.FlickerPlayPauseToggle();
        if (flickerLogic.isSpeedReading()) {
            toast("Started with interval " + flickerState.getPeriod());
        } else {
            toast("Stopped");
        }
        return true;
    }

    @Override
    public boolean onDoubleTap(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent e) {
        return false;
    }
}