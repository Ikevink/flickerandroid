package au.com.ikevinkit.flicker.controller;

/**
 * Created by Ikevink on 12/06/2015.
 */
public abstract class Conversion {
    public static int perMinuteToFrequency(int numPerMinute) {
        Double value = 60000.0 / numPerMinute;
        return value.intValue();
    }
}
