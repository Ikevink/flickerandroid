package au.com.ikevinkit.flicker.model;

import android.content.ContentValues;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Color;
import android.provider.BaseColumns;
import android.util.Log;

import au.com.ikevinkit.flicker.controller.FlickerDatabaseLogic;

/**
 * Created by Ikevink on 28/05/2015.
 */
public class DbHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 3;
    private static final String DATABASE_NAME = "FlickerDroid.db";

    //private static final String TABLE_CURRENT_BOOK = "CurBook"; // stores all the words in the current book indexed by the order in which they appear in the document
    /* Inner class that defines the table contents */
    public static abstract class CurrentBook implements BaseColumns {
        public static final String TABLE_NAME = "CurBook";
        public static final String COLUMN_NAME_ID = "id";
        public static final String COLUMN_NAME_PIECE_INDEX = "pieceid";
        public static final String COLUMN_NAME_WORD = "word";
    }

    /*private static final String CURRENT_BOOK_TABLE_CREATE =
            "CREATE TABLE " + CurrentBook.TABLE_NAME + " (" +
                    CurrentBook.COLUMN_NAME_ID + " NUMBER PRIMARY KEY, " +
                    CurrentBook.COLUMN_NAME_PIECE_INDEX + " NUMBER PRIMARY KEY, " +
                    CurrentBook.COLUMN_NAME_WORD + " TEXT);";*/
    private static final String CURRENT_BOOK_TABLE_CREATE =
            "CREATE TABLE " + CurrentBook.TABLE_NAME + " (" +
                    CurrentBook.COLUMN_NAME_ID + " NUMBER, " +
                    CurrentBook.COLUMN_NAME_PIECE_INDEX + " NUMBER, " +
                    CurrentBook.COLUMN_NAME_WORD + " TEXT, " +
                    " PRIMARY KEY (" + CurrentBook.COLUMN_NAME_ID + ", " + CurrentBook.COLUMN_NAME_PIECE_INDEX + "));";

    //private static final String TABLE_HISTORY = "History";      // stores a list of the indexes of the words you are up to in each book you've read
    public static abstract class BookHistory implements BaseColumns {
        public static final String TABLE_NAME = "History";
        public static final String COLUMN_NAME_PATH = "filepath";       // id of document
        public static final String COLUMN_NAME_UPTO = "upto";
    }

    private static final String HISTORY_TABLE_CREATE =
            "CREATE TABLE " + BookHistory.TABLE_NAME + " (" +
            BookHistory.COLUMN_NAME_PATH + " TEXT PRIMARY KEY, " +
            BookHistory.COLUMN_NAME_UPTO + " NUMBER);";

    public static abstract class TableSettings implements BaseColumns {
        public static final String TABLE_NAME = "Settings";
        public static final String COLUMN_NAME_SETTING = "setting";       // id of document
        public static final String COLUMN_NAME_VALUE = "V";
    }

    private static final String SETTINGS_TABLE_CREATE =
            "CREATE TABLE " + TableSettings.TABLE_NAME + " (" +
                    TableSettings.COLUMN_NAME_SETTING + " TEXT PRIMARY KEY, " +
                    TableSettings.COLUMN_NAME_VALUE + " TEXT);";

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //Log.d("DbHelper", "Generated new DB");
        db.execSQL(HISTORY_TABLE_CREATE);
        db.execSQL(CURRENT_BOOK_TABLE_CREATE);
        db.execSQL(SETTINGS_TABLE_CREATE);


        /*ContentValues values1 = new ContentValues();
        values1.put(DbHelper.TableSettings.COLUMN_NAME_SETTING, FlickerDatabaseLogic.WPM);
        values1.put(DbHelper.TableSettings.COLUMN_NAME_VALUE, 100);
        db.insert(TableSettings.TABLE_NAME, null, values1);

        ContentValues values2 = new ContentValues();
        values2.put(DbHelper.TableSettings.COLUMN_NAME_SETTING, FlickerDatabaseLogic.BACKGROUNDCOLOR);
        values2.put(DbHelper.TableSettings.COLUMN_NAME_VALUE, Color.BLACK);
        db.insert(TableSettings.TABLE_NAME, null, values2);

        ContentValues values3 = new ContentValues();
        values3.put(DbHelper.TableSettings.COLUMN_NAME_SETTING, FlickerDatabaseLogic.FONTCOLOR);
        values3.put(DbHelper.TableSettings.COLUMN_NAME_VALUE, Color.WHITE);
        db.insert(TableSettings.TABLE_NAME, null, values3);

        ContentValues values4 = new ContentValues();
        values4.put(DbHelper.TableSettings.COLUMN_NAME_SETTING, FlickerDatabaseLogic.MAXCHARACTERS);
        values4.put(DbHelper.TableSettings.COLUMN_NAME_VALUE, 16);
        db.insert(TableSettings.TABLE_NAME, null, values4);

        ContentValues values5 = new ContentValues();
        values5.put(DbHelper.TableSettings.COLUMN_NAME_SETTING, FlickerDatabaseLogic.ORIENTATION);
        values5.put(DbHelper.TableSettings.COLUMN_NAME_VALUE, ActivityInfo.SCREEN_ORIENTATION_SENSOR);
        db.insert(TableSettings.TABLE_NAME, null, values5);*/

        ContentValues values6 = new ContentValues();
        values6.put(DbHelper.TableSettings.COLUMN_NAME_SETTING, FlickerDatabaseLogic.LASTDOCUMENT);
        values6.put(DbHelper.TableSettings.COLUMN_NAME_VALUE, "def");
        db.insert(TableSettings.TABLE_NAME, null, values6);

        ContentValues values7 = new ContentValues();
        values7.put(DbHelper.TableSettings.COLUMN_NAME_SETTING, FlickerDatabaseLogic.LOADING);
        values7.put(DbHelper.TableSettings.COLUMN_NAME_VALUE, "F");
        db.insert(TableSettings.TABLE_NAME, null, values7);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.delete(TableSettings.TABLE_NAME, null, null);
        db.delete(CurrentBook.TABLE_NAME, null, null);
        db.delete(BookHistory.TABLE_NAME, null, null);
        onCreate(db);
    }
}
