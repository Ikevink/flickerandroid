package au.com.ikevinkit.flicker.model;

import android.database.Cursor;

/**
 * Created by ikevink on 7/13/15.
 */
public class FlickerWords {
    public int firstWordIndex;
    public String[] words;

    public FlickerWords(int firstWordIndex, String[] words) {
        this.firstWordIndex = firstWordIndex;
        this.words = words;
    }

    public FlickerWords(Cursor c) {
        c.moveToFirst();
        firstWordIndex = c.getInt(0);
        words = new String[c.getCount()];
        for (int i = 0; i < words.length; i++) {
            words[i] = c.getString(1);
            c.moveToNext();
        }
    }
}
