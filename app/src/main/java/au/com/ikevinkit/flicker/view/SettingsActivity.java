package au.com.ikevinkit.flicker.view;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.MultiSelectListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceGroup;
import android.preference.PreferenceManager;

import java.util.prefs.Preferences;

import au.com.ikevinkit.flicker.R;
import au.com.ikevinkit.flicker.controller.FlickerDatabaseLogic;

/**
 * Created by Ikevink on 12/06/2015.
 */
public class SettingsActivity extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    private int backgroundcolour;
    private int fontcolour;
    private int maxCharacters;      // how long a word can be before it is split across multiple pages
    private int wpm;
    private int orientation;

    private void returnSettings() {
        // get the Entered  message
        //String message=editTextMessage.getText().toString();
        Intent intentMessage=new Intent();


        // put the message to return as result in Intent
        intentMessage.putExtra(FlickerDatabaseLogic.BACKGROUNDCOLOR,backgroundcolour);
        intentMessage.putExtra(FlickerDatabaseLogic.FONTCOLOR,fontcolour);
        intentMessage.putExtra(FlickerDatabaseLogic.MAXCHARACTERS,maxCharacters);
        intentMessage.putExtra(FlickerDatabaseLogic.WPM, wpm);
        intentMessage.putExtra(FlickerDatabaseLogic.ORIENTATION, orientation);
        // Set The Result in Intent
        setResult(2, intentMessage);
        // finish The activity
        //finish();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        PreferenceManager.setDefaultValues(SettingsActivity.this, R.xml.preferences,
                false);
        initSummary(getPreferenceScreen());
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Set up a listener whenever a key changes
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Unregister the listener whenever a key changes
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
                                          String key) {
        updatePrefSummary(findPreference(key));
    }

    private void initSummary(Preference p) {
        if (p instanceof PreferenceGroup) {
            PreferenceGroup pGrp = (PreferenceGroup) p;
            for (int i = 0; i < pGrp.getPreferenceCount(); i++) {
                initSummary(pGrp.getPreference(i));
            }
        } else {
            updatePrefSummary(p);
        }
    }

    private void updatePrefSummary(Preference p) {
        if (p instanceof ListPreference) {
            ListPreference listPref = (ListPreference) p;
            p.setSummary(listPref.getEntry());
        }
        if (p instanceof EditTextPreference) {
            EditTextPreference editTextPref = (EditTextPreference) p;
            if (p.getTitle().toString().contains("assword"))
            {
                p.setSummary("******");
            } else {
                p.setSummary(editTextPref.getText());
            }
        }
        if (p instanceof MultiSelectListPreference) {
            EditTextPreference editTextPref = (EditTextPreference) p;
            p.setSummary(editTextPref.getText());
        }
    }
}