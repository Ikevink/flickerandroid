package au.com.ikevinkit.flicker.controller;

import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import au.com.ikevinkit.flicker.exceptions.EmptyBookException;
import au.com.ikevinkit.flicker.exceptions.InvalidInputException;
import au.com.ikevinkit.flicker.exceptions.InvalidSettingException;
import au.com.ikevinkit.flicker.exceptions.NoLastDocException;
import au.com.ikevinkit.flicker.exceptions.NotInitialisedException;
import au.com.ikevinkit.flicker.model.FlickerState;

/**
 * Created by Ikevink on 26/05/2015.
 */
public class FlickerLogic {
    //final Handler handler = new Handler();

    private FlickerState flickerState;
    private Timer wordTimer;
    private TimerTask wordTimerTask;
    public boolean isSpeedReading() {
        return wordTimer != null;
    }


    public FlickerLogic(FlickerState flickerState) {
        this.flickerState = flickerState;
    }


    private int wordPieceIndex;     // reset to 0 every time the word index is changed

    public static String[] SplitTextIntoWords(String s) {
        //if(!String.IsNullOrEmpty(s))
        if (StringUtils.isNotEmpty(s) && StringUtils.isNotBlank(s))
            return s.split("[ ,;-]+");
        //,System.StringSplitOptions.RemoveEmptyEntries);

        String string[] = {"No text."};
        return string;
    }

    public static String GenerateWhiteSpacePadding(int length) {
        String padding = "";
        for (int i = 0; i < length; i++)
        {
            //padding += "\u00A0";
            padding += "&#160;";
            //padding += "&nbsp;";
            //padding += " ";
        }
        return padding;
    }

    public static String GenerateNextString(String word, Boolean withFormatting)
    {
        int midIndex = FindBestCentre(word);

        if (word.length() == 0)
            return "";

        String leftPhrase = midIndex == 0 ? "" : word.substring(0, midIndex);

        String midPhrase = word.substring(midIndex, midIndex + 1);

        String rightPhrase = midIndex == word.length() - 1 ? "" : word.substring(midIndex + 1, word.length());

        if (withFormatting)
        {
            int leftPadding = 15 - leftPhrase.length();
            int rightPadding = 15 - rightPhrase.length();
            int fontSize = rightPhrase.length() + midPhrase.length() + leftPhrase.length() > 15 ? 15 : 20;
            return  "<body style=\"text-align:center\">"
                    + "<font size=\""+Integer.toString(fontSize)+"\">"
                    + GenerateWhiteSpacePadding(leftPadding)
                    + leftPhrase
                    + "<font color=\"red\">"
                    + midPhrase
                    + "</font>"
                    + rightPhrase
                    + GenerateWhiteSpacePadding(rightPadding)
                    + "</font>"
                    + "</body>";
        }

        return leftPhrase + "|" + midPhrase + "|" + rightPhrase;
    }

    public static int FindBestCentre(String word)
    {
        int length = word.length();

        if (length < 3)
            return 0;

        if (length == 3)
            return 1;

        int firstVowelIndex = word.length() - 1;
        int secondVowelIndex = word.length() - 1;

        int vowelCount = 0;
        for (int i = word.length() - 1; i >= 0; i--) {
            char ch = word.charAt(i);
            if (ch == 'a' || ch == 'A' || ch == 'e' || ch == 'E' || ch == 'i' || ch == 'I' || ch == 'o' || ch == 'O' || ch == 'u' || ch == 'U') {
                vowelCount++;
                secondVowelIndex = firstVowelIndex;
                firstVowelIndex = i;

            }
        }

        if (vowelCount == 0) {
            Double value = word.length() / 3.0;
            value = Math.floor(value);
            return value.intValue();
        }

        int middleOfWord = word.length() / 2; //if the selected vowel is really late in a long word, just go with the middle of the word - eg "Sec*t*ion"  instead of "Sect*i*on"

        if (vowelCount < 3) {
            return Math.min(middleOfWord, firstVowelIndex);
        }

        return Math.min(middleOfWord, secondVowelIndex);
    }

    public static String[] ReadFileAsStringArray(File file)
    {
        return SplitTextIntoWords(ReadFileAsString(file));
    }

    public static String ReadFileAsString(File file)
    {
        String filetext = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String receiveString = "";
            StringBuilder stringBuilder = new StringBuilder();

            while ((receiveString = br.readLine()) != null) {
                stringBuilder.append(receiveString);
            }

            br.close();
            filetext = stringBuilder.toString();
        } catch (Exception e) {

        } finally {
        }

        return filetext;
    }

    public String[] formatWordSplit(String word) {
        String[] splitWord = new String[3];
        if (word.length() == 0)
            return splitWord;

        int midIndex = FindBestCentre(word);
        splitWord[0] = midIndex == 0 ? "" : word.substring(0, midIndex);

        splitWord[1] = word.substring(midIndex, midIndex + 1);

        splitWord[2] = midIndex == word.length() - 1 ? "" : word.substring(midIndex + 1, word.length());

        return splitWord;
    }

    public String getNextWord() throws NotInitialisedException, EmptyBookException, NoLastDocException {
        //flickerState.nextWord();
        return GenerateNextString(flickerState.getCurrentWord(), true);
    }

    public String[] getNextWordSplit() throws NotInitialisedException, EmptyBookException, NoLastDocException {
        //flickerState.nextWord();
        return formatWordSplit(flickerState.getCurrentWord());
        //return GenerateNextString(flickerState.getCurrentWord(), true);
    }

    public void pause() {
        if (wordTimer != null) {
            wordTimer.cancel();
            wordTimer = null;
        }
    }

    public void play() {
        if (wordTimer == null) {
            //set a new Timer
            wordTimer = new Timer();
            //initialize the TimerTask's job
            initializeWordTimerTask();
            //schedule the timer, after the first 5000ms the TimerTask will run every 10000ms
            wordTimer.schedule(wordTimerTask, 1000, flickerState.getPeriod()); //
            //return true;
        }
    }

    // returns true if it just started, false if it paused
    public void FlickerPlayPauseToggle()
    {
        if (wordTimer != null) {
            pause();
        } else {
            play();
        }
    }

    /*public void initializeWordTimerTask() {
        wordTimerTask = new TimerTask() {
            public void run() {
                //use a handler to run a toast that shows the current timestamp
                handler.post(new Runnable() {
                    public void run() {
                        try {
                            flickerState.nextWord();
                        } catch (NotInitialisedException e) {
                            e.printStackTrace();
                        } catch (NoLastDocException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        };
    }*/

    public void initializeWordTimerTask() {
        wordTimerTask = new TimerTask() {
            public void run() {
                //use a handler to run a toast that shows the current timestamp
                Handler handler = new Handler();
                handler.post(new Runnable() {
                    public void run() {
                        try {
                            flickerState.nextWord();
                        } catch (NotInitialisedException e) {
                            e.printStackTrace();
                        } catch (NoLastDocException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        };
    }

    /* 3 options
     *   - last document is this document
     *   - this document has been opened before
     *   - this is a new document
     */
    public void loadFile(File file) throws NotInitialisedException {

        boolean lastDocLoadedFully = true;
        try {
            lastDocLoadedFully = FlickerDatabaseLogic.getSetting(FlickerDatabaseLogic.LOADING).equals("F");
        } catch (InvalidSettingException e) {
            e.printStackTrace();
        }
        if (isLastDocumentSame(file) && lastDocLoadedFully) {

        } else {//if (isNewBook(file)) {
            try {
                FlickerDatabaseLogic.setSetting(FlickerDatabaseLogic.LOADING, "T");
            } catch (NotInitialisedException e) {
                e.printStackTrace();
            }
            new loadDocWordByWordIntoDatabaseTask().execute(file);
            //loadWordsIntoDatabase(FlickerLogic.ReadFileAsStringArray(file));
            FlickerDatabaseLogic.setLastDoc(file.getPath());
            //if (getBookProgress(file.getPath()) == -1) {
            //    newBook(file.getPath());
            //}
        }
    }

    public boolean isLastDocumentSame(File file) throws NotInitialisedException {
        try {
            if (FlickerDatabaseLogic.getLastDoc().equals(file.getPath())) {
                return true;
            }
        } catch (NoLastDocException ex) {
            return false;
        }
        return false;
    }

    private class loadDocWordByWordIntoDatabaseTask extends AsyncTask<File, Void, Void> {
        int tarProgress = 0;
        @Override
        protected Void doInBackground(File... params) {
            File file = params[0];
            try {
                tarProgress = FlickerDatabaseLogic.getBookProgress();
            } catch (NoLastDocException e) {
                e.printStackTrace();
                tarProgress = 0;
            } catch (NotInitialisedException e) {
                e.printStackTrace();
                tarProgress = 0;
            }
            try {
                FlickerDatabaseLogic.setSetting(FlickerDatabaseLogic.LOADING, "T");
            } catch (NotInitialisedException e) {
                e.printStackTrace();
            }

            int nextWordToLoadIndex = 0;
            BufferedReader br = null;
            try {
                br = new BufferedReader(new FileReader(file));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            //StringBuilder stringBuilder = new StringBuilder();

            FlickerDatabaseLogic.eraseCurrentBook();

            if (br != null) {
                try {
                    String receiveString = "";
                    while ((receiveString = br.readLine()) != null) {
                        String words[] = receiveString.split("[ ,;-]+");
                        for (int i = 0; i < words.length; i++) {
                            if (StringUtils.isNotEmpty(words[i]) && StringUtils.isNotBlank(words[i])) {
                                String word = words[i];
                                while (word.length() > 13)
                                /*if (words[i].length() > 13) {
                                    for (int wordSplittingIndex = 0; wordSplittingIndex < words[i].length(); wordSplittingIndex += 12) {

                                    }
                                } else {
                                    FlickerDatabaseLogic.addWordAt(nextWordToLoadIndex, words[i]);
                                    if (tarProgress == nextWordToLoadIndex) {
                                        flickerState.loadSavedState();
                                    }
                                }*/
                                nextWordToLoadIndex++;
                            }
                        }

                    }
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            try {
                FlickerDatabaseLogic.setSetting(FlickerDatabaseLogic.LOADING, "F");
            } catch (NotInitialisedException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

}
