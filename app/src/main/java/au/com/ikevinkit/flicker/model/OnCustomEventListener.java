package au.com.ikevinkit.flicker.model;

/**
 * Created by Ikevink on 27/05/2015.
 */
public interface OnCustomEventListener {
    public void onEvent();
}
