package au.com.ikevinkit.flicker.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import au.com.ikevinkit.flicker.controller.Conversion;
import au.com.ikevinkit.flicker.controller.FlickerDatabaseLogic;
import au.com.ikevinkit.flicker.exceptions.EmptyBookException;
import au.com.ikevinkit.flicker.exceptions.InvalidSettingException;
import au.com.ikevinkit.flicker.exceptions.NoLastDocException;
import au.com.ikevinkit.flicker.exceptions.NotInitialisedException;

/**
 * Created by Ikevink on 26/05/2015.
 * This class fires events out that are picked up by the view (activity)
 */
public class FlickerState {
    OnCustomEventListener wordChangedListener;
    public void wordChangedListener(OnCustomEventListener eventListener) {
        wordChangedListener = eventListener;
    }

    private int period;

    public int getPeriod() {
        return this.period;
    }

    // default constructor used mainly for testing crap prior to implementing file reading
    public FlickerState(Context context) {
        FlickerDatabaseLogic.initialise(context);
        period = 100;

        loadSettings(PreferenceManager.getDefaultSharedPreferences(context));
    }


    // populates the initial value of displayTextIndex
    public void loadSavedState() {
        if(wordChangedListener!=null) {
            wordChangedListener.onEvent();
        }
    }

    public void loadSettings(SharedPreferences sharedPref) {
        period = Conversion.perMinuteToFrequency(sharedPref.getInt(FlickerDatabaseLogic.WPM, 100));
    }

    public void nextWord() throws NoLastDocException, NotInitialisedException {
        //if (displayTextIndex < displayText.length) {
            FlickerDatabaseLogic.incrementWord();
            if(wordChangedListener!=null) {
                wordChangedListener.onEvent();
            }
        //}
    }

    public String getCurrentWord() throws EmptyBookException, NotInitialisedException, NoLastDocException {
        FlickerWords words = FlickerDatabaseLogic.getWords(1);
        return words.words[0];
    }
}