package au.com.ikevinkit.flicker.model;

import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;

import au.com.ikevinkit.flicker.events.WordSelectedListener;

/**
 * Created by ikevink on 7/13/15.
 */
public final class WordIndexedClickableSpan extends ClickableSpan {
    public static WordSelectedListener listener;
    private int wordIndex;

    public WordIndexedClickableSpan(int wordIndex) {
        this.wordIndex = wordIndex;
    }

    @Override
    public void onClick(View widget) {
        //Log.d("abc", Integer.toString(wordIndex));
        listener.wordSelected(wordIndex);
    }

    public void updateDrawState(TextPaint ds) {// override updateDrawState
        ds.setUnderlineText(false); // set to false to remove underline
    }
}