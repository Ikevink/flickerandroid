package au.com.ikevinkit.flicker.controller;

import android.test.AndroidTestCase;

import junit.framework.Assert;

import au.com.ikevinkit.flicker.exceptions.NoWordsException;

import static org.junit.Assert.*;

/**
 * Created by ikevink on 7/29/15.
 */
public class EnglishTest extends AndroidTestCase {
    public void testSplitWordOnMathematicallyEven() throws Exception {

    }

    public void testSplitWordOnSyllableEvenly() throws Exception {

    }

    public void testSplitWordOnFrontLoad() throws Exception {
        assertEquals("abc",           English.splitWordOnFrontLoad("abc", 13)[0]);

        assertEquals("abcdefghijklm", English.splitWordOnFrontLoad("abcdefghijklm", 13)[0]);

        assertEquals("abcdefghijkl-", English.splitWordOnFrontLoad("abcdefghijklmn", 13)[0]);
        assertEquals("mn",            English.splitWordOnFrontLoad("abcdefghijklmn", 13)[1]);

        assertEquals("abcdefghijkl-", English.splitWordOnFrontLoad("abcdefghijklmnopqrstuvwxy", 13)[0]);
        assertEquals("mnopqrstuvwxy", English.splitWordOnFrontLoad("abcdefghijklmnopqrstuvwxy", 13)[1]);

        assertEquals("abcdefghijkl-", English.splitWordOnFrontLoad("abcdefghijklmnopqrstuvwxyz", 13)[0]);
        assertEquals("mnopqrstuvwx-", English.splitWordOnFrontLoad("abcdefghijklmnopqrstuvwxyz", 13)[1]);
        assertEquals("yz",            English.splitWordOnFrontLoad("abcdefghijklmnopqrstuvwxyz", 13)[2]);
    }

    // logic keeps changing
    public void testSplitWords() throws Exception {

    }

    public void testGetMinSplitsInWord() throws Exception {
        assertEquals(1, English.getMinSplitsInWord(13, 13));
        //assertEquals(1, English.getMinSplitsInWord(13, 13));
        try {
            English.getMinSplitsInWord(0, 13);
            Assert.fail("Expected NoWordsException");
        } catch (NoWordsException ex) {
            // success
        }

        assertEquals(2, English.getMinSplitsInWord(14, 13));
        assertEquals(2, English.getMinSplitsInWord(24, 13));
        assertEquals(2, English.getMinSplitsInWord(25, 13));
        assertEquals(3, English.getMinSplitsInWord(26, 13));
    }
}