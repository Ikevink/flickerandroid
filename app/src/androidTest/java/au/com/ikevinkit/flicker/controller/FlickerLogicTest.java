package au.com.ikevinkit.flicker.controller;

import android.test.AndroidTestCase;
import android.test.RenamingDelegatingContext;

import java.io.File;

import au.com.ikevinkit.flicker.model.FlickerState;

/**
 * Created by Ikevink on 15/06/2015.
 */
public class FlickerLogicTest extends AndroidTestCase {
    FlickerLogic fl;
    FlickerState fs;

    public void setUp() throws Exception {
        super.setUp();
        RenamingDelegatingContext context = new RenamingDelegatingContext(getContext(), "test_");

        fs = new FlickerState(context);
        fl = new FlickerLogic(fs);
        FlickerDatabaseLogic.initialise(context);
        FlickerDatabaseLogic.loadWordsIntoDatabase(new String[]
                {"There", "was", "a", "fisherman", "named", "Fisher",
                "who", "fished", "for", "some", "fish", "in", "a", "fissure.",
                "Till", "a", "fish", "with", "a", "grin,",
                "pulled", "the", "fisherman", "in.",
                "Now", "they're", "fishing", "the", "fissure", "for", "Fisher."});

    }

    public void tearDown() throws Exception {

    }

    public void testIsSpeedReading() throws Exception {
        assert(fl.isSpeedReading() == false);
        fl.FlickerPlayPauseToggle();
        assert(fl.isSpeedReading() == true);
        fl.FlickerPlayPauseToggle();
        assert(fl.isSpeedReading() == false);
    }

    public void testSplitTextIntoWords() throws Exception {
        /*assert(FlickerLogic.FindBestCentre("") == 0);
        assert(FlickerLogic.FindBestCentre("t") == 0);
        assert(FlickerLogic.FindBestCentre("ttt") == 1);
        assert(FlickerLogic.FindBestCentre("ttttttttt") == 3);
        assert(FlickerLogic.FindBestCentre("atttttatt") == 0);
        assert(FlickerLogic.FindBestCentre("aaaaaaaaa") == 1);
        assert(FlickerLogic.FindBestCentre("attattatt") == 3);
        assert(FlickerLogic.FindBestCentre("atttttaat") == 4);*/
    }

    public void testGenerateWhiteSpacePadding() throws Exception {

    }

    public void testGenerateNextString() throws Exception {

    }

    public void testFindBestCentre() throws Exception {
        assertEquals(0, FlickerLogic.FindBestCentre(""));
        assertEquals(0, FlickerLogic.FindBestCentre("t"));
        assertEquals(1, FlickerLogic.FindBestCentre("ttt"));
        assertEquals(3, FlickerLogic.FindBestCentre("ttttttttt"));
        assertEquals(0, FlickerLogic.FindBestCentre("atttttatt"));
        assertEquals(1, FlickerLogic.FindBestCentre("aaaaaaaaa"));
        assertEquals(3, FlickerLogic.FindBestCentre("attattatt"));
        assertEquals(4, FlickerLogic.FindBestCentre("atttttaat"));
    }

    public void testReadFileAsStringArray() throws Exception {

    }

    public void testReadFileAsString() throws Exception {

    }

    /* getNextWord returns a formatted word for the case where html is fed through to the edittext */
    public void testGetNextWord() throws Exception {

    }

    public void testIsLastDocumentSame() throws Exception {
        FlickerDatabaseLogic.setLastDoc("qwerty");
        assertEquals(true,  fl.isLastDocumentSame(new File("qwerty")));
        assertEquals(false, fl.isLastDocumentSame(new File("abcdef")));
    }

    /* getNextWordSplit returns a length 3 array with no formatting containing the lhs, middle and rhs
     * where 3 individual edittexts perform the various formatting
     */
    public void testGetNextWordSplit() throws Exception {
        //String word = fs.getCurrentWord("");
        //word.
        String[] results = fl.formatWordSplit("");
        assert(results[0].equals("") && results[1].equals("") && results[2].equals(""));
        results = fl.formatWordSplit("a");
        assert(results[0].equals("") && results[1].equals("a") && results[2].equals(""));
        results = fl.formatWordSplit("t");
        assert(results[0].equals("") && results[1].equals("t") && results[2].equals(""));
        results = fl.formatWordSplit("the");
        assert(results[0].equals("t") && results[1].equals("h") && results[2].equals("e"));
        results = fl.formatWordSplit("attattatt");
        assert(results[0].equals("att") && results[1].equals("a") && results[2].equals("ttatt"));
    }

    public void testFlickerPlayPauseToggle() throws Exception {

    }

    public void testInitializeWordTimerTask() throws Exception {

    }
}