package au.com.ikevinkit.flicker.controller;

import android.test.AndroidTestCase;
import android.test.RenamingDelegatingContext;
import android.util.Log;

import java.io.File;

import au.com.ikevinkit.flicker.model.FlickerWords;

public class FlickerDatabaseLogicTest extends AndroidTestCase {
    @Override
    public void setUp() throws Exception {
        super.setUp();
        RenamingDelegatingContext context = new RenamingDelegatingContext(getContext(), "test_");
        FlickerDatabaseLogic.initialise(context);
    }

    @Override
    public void tearDown() throws Exception {
        super.tearDown();
    }

    /*public void testStoreSettings() throws Exception {
        flickerDatabaseLogic.debugTableSettings();

        Intent intent = new Intent();
        intent.putExtra(FlickerDatabaseLogic.BACKGROUNDCOLOR, Color.YELLOW);
        intent.putExtra(FlickerDatabaseLogic.FONTCOLOR, Color.BLUE);
        intent.putExtra(FlickerDatabaseLogic.MAXCHARACTERS, 12);
        intent.putExtra(FlickerDatabaseLogic.ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        intent.putExtra(FlickerDatabaseLogic.WPM, 200);

        flickerDatabaseLogic.storeSettings(intent);


        assertEquals(12, Integer.parseInt(flickerDatabaseLogic.getSetting(FlickerDatabaseLogic.MAXCHARACTERS)));
        assertEquals(200, Integer.parseInt(flickerDatabaseLogic.getSetting(FlickerDatabaseLogic.WPM)));
        assertEquals(Color.YELLOW, Integer.parseInt(flickerDatabaseLogic.getSetting(FlickerDatabaseLogic.BACKGROUNDCOLOR)));
        assertEquals(Color.BLUE, Integer.parseInt(flickerDatabaseLogic.getSetting(FlickerDatabaseLogic.FONTCOLOR)));
        assertEquals(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE, Integer.parseInt(flickerDatabaseLogic.getSetting(FlickerDatabaseLogic.ORIENTATION)));

    }*/


    public void testSetLastDocument() throws Exception {
        FlickerDatabaseLogic.setLastDoc("qwerty");
        assertEquals("qwerty", FlickerDatabaseLogic.getLastDoc());
        FlickerDatabaseLogic.setLastDoc("abcdef");
        assertEquals("abcdef", FlickerDatabaseLogic.getLastDoc());
        FlickerDatabaseLogic.setLastDoc("zyx");
        assertEquals("zyx", FlickerDatabaseLogic.getLastDoc());
    }

    public void testSetBookProgress() throws Exception {
        FlickerDatabaseLogic.setLastDoc("qwerty");
        FlickerDatabaseLogic.getBookProgress();
        FlickerDatabaseLogic.setBookProgress(5);
        assertEquals(5, FlickerDatabaseLogic.getBookProgress());

        FlickerDatabaseLogic.getBookProgress("abcdef");
        FlickerDatabaseLogic.setBookProgress("abcdef", 3);
        assertEquals(3, FlickerDatabaseLogic.getBookProgress("abcdef"));

        assertEquals("Reading non-active books progress returned wrongly", 5, FlickerDatabaseLogic.getBookProgress("qwerty"));
        FlickerDatabaseLogic.setBookProgress("qwerty", 1);
        assertEquals("Reading non-active books progress returned wrongly after changing it", 1, FlickerDatabaseLogic.getBookProgress("qwerty"));


        FlickerDatabaseLogic.setLastDoc("zyx");
        FlickerDatabaseLogic.getBookProgress("zyx");
        FlickerDatabaseLogic.setBookProgress(2);
        assertEquals(2, FlickerDatabaseLogic.getBookProgress());

        FlickerDatabaseLogic.setLastDoc("fghi");
        FlickerDatabaseLogic.getBookProgress("fghi");
        FlickerDatabaseLogic.setBookProgress("fghi", 4);
        assertEquals(4, FlickerDatabaseLogic.getBookProgress());
    }

    public void testNewBook() throws Exception {
        FlickerDatabaseLogic.newBook("ghi");
        assertEquals(0, FlickerDatabaseLogic.getBookProgress("ghi"));
    }

    public void testLoadFile() throws Exception {

    }



    public void testGetWords() throws Exception {
        FlickerDatabaseLogic.setLastDoc("qwerty");
        FlickerDatabaseLogic.getBookProgress();
        FlickerDatabaseLogic.loadWordsIntoDatabase(new String[]{"This", "stupid", "thing", "is", "driving", "me", "crazy", "because", "it", "is", "returning", "way", "too", "many", "words"});
        assertEquals("This", FlickerDatabaseLogic.getWord());

        FlickerWords words = FlickerDatabaseLogic.getWords(5);
        assertEquals(0, words.firstWordIndex);
        assertEquals(3, words.words.length);
    }

    /* removed the spacing between words in this because coming up with long words is too hard */
    public void testGetWordsLongWords() throws Exception {
        FlickerDatabaseLogic.setLastDoc("qwerty");
        FlickerDatabaseLogic.getBookProgress();
        FlickerDatabaseLogic.loadWordsIntoDatabase(new String[]{"Today", "wewillconsiderthe", "peculiar", "disadvantagesinherent",
                "in", "pontificating", "at", "great", "length", "on", "obscure", "Egyptian", "hieroglyphics", "to ", "a", "class", "of", "remedial",
                "students", "with", "a", "disastrously", "abbreviated", "attention", "span", "and", "a", "lamentable", "absence", "of", "both",
                "intellect", "and ", "industry,", "leading", "inevitably", "to", "a", "complete", "lack", "of", "comprehension", "from", "them,",
                "and", "incipient", "despair", "in", "the", "pedagogue.",
        });
        assertEquals("Today", FlickerDatabaseLogic.getWord());
        FlickerDatabaseLogic.incrementWord();

        // first word that was divided up
        assertEquals("wewillconside-", FlickerDatabaseLogic.getWord());
        FlickerDatabaseLogic.incrementWord();
        assertEquals("rthe", FlickerDatabaseLogic.getWord());
        FlickerDatabaseLogic.incrementWord();

        assertEquals("peculiar", FlickerDatabaseLogic.getWord());
        FlickerDatabaseLogic.incrementWord();

        // second word that was divided up
        assertEquals("disadvantage-", FlickerDatabaseLogic.getWord());
        FlickerDatabaseLogic.incrementWord();
        assertEquals("sinherent", FlickerDatabaseLogic.getWord());
        FlickerDatabaseLogic.incrementWord();
    }
}